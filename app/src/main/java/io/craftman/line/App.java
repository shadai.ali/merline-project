package io.craftman.line;

import android.app.Application;
import android.content.Context;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 20/02/2017.
 */

public class App extends Application {

    static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (this){
            mContext = this;
        }
    }

    public static Context getContext() {
        return mContext;
    }
}
