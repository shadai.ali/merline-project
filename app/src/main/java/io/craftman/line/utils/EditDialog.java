package io.craftman.line.utils;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import io.craftman.line.R;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 18/03/2017.
 */

public class EditDialog extends AppCompatDialogFragment {

    EditText editText;
    Activity activity;
    String adminmdp = "12345678";
    PassListener passListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v;
         v = inflater.inflate(R.layout.dialog_pass,container,false);
         editText = (EditText) v.findViewById(R.id.pass);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String ed = editable.toString().trim();

                if ( 8 <= ed.length()){
                    // pass is correct
                    if( ed.equalsIgnoreCase(adminmdp)){
                       passListener.isCorrect();
                        dismiss();
                    }
                    else {
                        // pass isn't correct
                        editText.setError(getString(R.string.incorrect_pass));

                    }
                }
            }
        });

    }

    public void show(Activity activity, FragmentManager fragmentManager, @NonNull PassListener listener){
        this.activity = activity;
        this.passListener = listener;
        show(fragmentManager,"ouloulou");
    }

    public interface PassListener{
         void isCorrect();
    }




}
