package io.craftman.line.utils;

import android.webkit.URLUtil;
import android.widget.Toast;

import org.greenrobot.essentials.io.FileUtils;

import java.io.File;
import java.io.IOException;

import io.craftman.line.App;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 10/03/2017.
 */

public class TextUtils {

    public static  String fileToString(File mFile){
        try {
            return FileUtils.readUtf8(mFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "CONTENT ERROR";
    }


    public static boolean isValidUrl(String url){
       return URLUtil.isValidUrl(url);
    }

    public static void toast(String msg){
        Toast.makeText(App.getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
