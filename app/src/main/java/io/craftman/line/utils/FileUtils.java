package io.craftman.line.utils;

import android.content.Context;

import java.io.File;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 12/03/2017.
 */

public class FileUtils {


    /**
     * Juste un utilitaire pour verifier si un fichier existe
     * @param filename
     * @return
     */
    public static boolean exist(File dir,String filename){
        File f  = new File(dir,filename);
        return f.exists() && !f.isDirectory();

    }

    /**
     * verifie si un fichier existe dans le repertoire interne de l'application
     * @param filename
     * @param ctx
     * @return
     */
    public static boolean exist(String filename, Context ctx){
        return exist(ctx.getFilesDir(),filename);
    }


    /**
     * permet de recuperer un fichier si il existe bien - sûr
     * @param filename
     * @param ctx
     * @return
     */
    public static File getFile(String filename, Context ctx){
        return new File(ctx.getFilesDir(),filename);
    }
}
