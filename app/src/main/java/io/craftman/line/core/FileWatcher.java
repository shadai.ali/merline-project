package io.craftman.line.core;

import android.content.Context;
import android.util.Log;

import org.greenrobot.essentials.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Map;

import io.craftman.line.App;
import io.craftman.line.R;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.webkit.URLUtil.isValidUrl;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 19/02/2017.
 */

public class FileWatcher {

    private static final String TAG = "FileWatcher";
    String[] remoteFileUrl;
    OkHttpClient okHttpClient = new OkHttpClient();
    Request request;
    String serverUrl;
    String serverIP;
    String httpPort;
    Map<String, String> filesUrlAndHeader;
    Socket socket;
    private OnFileChangeListener mOnFileChangeListener;

    public FileWatcher(OnFileChangeListener onFileChangeListener, String... remoteFileUrls) {
        remoteFileUrl = remoteFileUrls;
        mOnFileChangeListener = onFileChangeListener;
        watchFiles();
    }

    public FileWatcher(OnFileChangeListener onFileChangeListener, String wacthingServerUrl, String listeenPort, String httpPort) {
        serverUrl = wacthingServerUrl + ':' + listeenPort;
        serverIP = wacthingServerUrl;
        mOnFileChangeListener = onFileChangeListener;
        this.httpPort = httpPort;
        setupSocketListeners();
    }


    private void setupSocketListeners() {
        try {
            socket = IO.socket(serverUrl);
            socket.connect()


            .on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Log.e(TAG, "socket is connected");
                }

            }).on("update", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Log.e(TAG,"update m" + args[0].toString());
                    try {
                        handleFileChanges(new JSONObject(args[0].toString()) );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Log.e(TAG, "socket is disconnected");
                }

            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG,args.toString());
                }
            })
            .on(Socket.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG,args.toString());
                }
            });


        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e(TAG, serverUrl + " is invalid server url");
        }
    }

    private void handleFileChanges(JSONObject jsonObject) throws JSONException {
        if (jsonObject == null) return;
        downloadFile(serverIP, jsonObject.getString("name")
                , jsonObject.getString("extension"),
                new DownloadCallBack() {
                    @Override
                    public void onSucces(File file,String name) {
                        mOnFileChangeListener.onFileChanged(file,name);
                    }

                    @Override
                    public void onFailed(String error) {

                    }
                });
    }

    private void watchFiles() {

        for (String url : remoteFileUrl) {
            if (isValidUrl(url)) {
                try {
                    addWatcher(url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void addWatcher(String url) throws IOException {

        request = new Request.Builder()
                .url(url)
                .build();

        Response response = okHttpClient.newCall(request).execute();

       /* if (response.header("last-modified").trim().equalsIgnoreCase()){

        }*/

    }

    void downloadFile(String url, final String filename, final String fileExt, final DownloadCallBack cb) {
       url += ':' + httpPort + '/'  + filename + '.' + fileExt;
        request = new Request.Builder()
                .url(url)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                cb.onFailed(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String charSequence  = new String(response.body().bytes(), Charset.forName("UTF-8"));
                Log.e(TAG,charSequence);

                File file = File.createTempFile(filename, fileExt, App.getContext().getCacheDir());
                FileUtils.writeUtf8(file,charSequence);
                cb.onSucces(file,filename);
            }
        });
    }
   public  static void saveFileFromUrl(final String fullUrl,final String filename, final Context ctx, final DownloadCallBack cb) {
        Request request = new Request.Builder()
                .url(fullUrl)
                .build();

       OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                cb.onFailed(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if( response.isSuccessful()) {
                    String charSequence = new String(response.body().bytes(), Charset.forName("UTF-8"));
                    Log.e(TAG, charSequence);

                    File file = new File(ctx.getFilesDir(), filename);
                    FileUtils.writeUtf8(file, charSequence);
                    cb.onSucces(file, filename);
                }
                
                else   cb.onFailed(ctx.getString(R.string.file404));
            }
        });
    }


    /**
     * Remote file change watcher
     * use this lisner for get notified when the remote file change
     */
    public interface OnFileChangeListener {

        /**
         * when the file change the new version is passed to this method
         * paramters
         *
         * @File mfile new version from remote server
         */
        void onFileChanged(File mfile, String name);
    }


    public interface DownloadCallBack {
        void onSucces(File file, String name);

        void onFailed(String error);
    }
}
