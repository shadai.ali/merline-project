package io.craftman.line.business.config;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 10/03/2017.
 */

public class Network {

    /** Adresse du serveur de fichier */
    public static String END_POINT = "http://159.203.91.203";

    /** port d'ecoute sur le serveur des communications par socket */
    public static String SOCKET_PORT = "1337";

    /** port d'ecoute sur le serveur des communications par HTTP */
    public static String HTTP_PORT = "8080";



    public static String url(String filepath){
        return END_POINT + ":" +HTTP_PORT + "/" + filepath;
    }

}
