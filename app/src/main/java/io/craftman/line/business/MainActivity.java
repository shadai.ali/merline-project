package io.craftman.line.business;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.File;

import io.craftman.line.R;
import io.craftman.line.core.FileWatcher;
import io.craftman.line.utils.EditDialog;
import io.craftman.line.utils.FileUtils;

import static io.craftman.line.business.config.DefaultFiles._BILAN_DAY;
import static io.craftman.line.business.config.DefaultFiles._BILAN_HEBDO;
import static io.craftman.line.business.config.DefaultFiles._BILAN_MONTH;
import static io.craftman.line.business.config.DefaultFiles._CAISSE_DAY;
import static io.craftman.line.business.config.DefaultFiles._CAISSE_HEBDO;
import static io.craftman.line.business.config.DefaultFiles._CAISSE_MONTH;
import static io.craftman.line.business.config.DefaultFiles._STOCK_DAY;
import static io.craftman.line.business.config.DefaultFiles._STOCK_HEBDO;
import static io.craftman.line.business.config.DefaultFiles._STOCK_MONTH;
import static io.craftman.line.business.config.DefaultFiles._VENTE_DAY;
import static io.craftman.line.business.config.DefaultFiles._VENTE_HEBDO;
import static io.craftman.line.business.config.DefaultFiles._VENTE_MONTH;
import static io.craftman.line.business.config.Network.END_POINT;
import static io.craftman.line.business.config.Network.HTTP_PORT;
import static io.craftman.line.business.config.Network.SOCKET_PORT;
import static io.craftman.line.business.config.Network.url;
import static io.craftman.line.utils.TextUtils.fileToString;

public class MainActivity extends AppCompatActivity
         {

    private static final long MVM_JOUR = 2000;
    private static final long MVM_HEBDO = 2001;
    private static final long MVM_MONTH = 2002;
    private static final long MVMC_JOUR = 2003;
    private static final long MVMC_HBDO = 2004;
    private static final long MVMC_MONTH = 2005;
    private static final long V_JOUR = 2006;
    private static final long V_HEBDO = 2007;
    private static final long V_MONTH = 2008;
    private static final long B_JOUR = 2009;
    private static final long B_HEBDO = 2010;
    private static final long B_MONTH = 2011;
    WebView contentView;
    Toolbar toolbar;
    private ProgressDialog progresDialog;
    boolean fileIsShowing;
    private String currentFile;
    private AccountHeader headerResult;
    private Drawer result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fileIsShowing = false;
        setupUiFrame();
       // new DrawerBuilder().withActivity(this).build();
        buildHeader(false, savedInstanceState);
        buildDrawer(savedInstanceState);

        manageMPermissions();

        inflateViews();
        setupFileWatechDeamon();
    }

    private void setupFileWatechDeamon() {
        /**
         * File Witcher is Java class i have build for Listeen any change of my remotes files
         *  it logic is so simple. U don't need to understand how it work
         *  but how u can use it.
         *  It based on event programming
         *  So u just need to give it an CallBack <class> OnFileChangeListener </class>
         *  and when ur remote file change it'll call onFileChanged(File mfile)
         *  where @param File is ur file in UTF-8 encoding no encryption, bytes read from base64
         */
        new FileWatcher(new FileWatcher.OnFileChangeListener() {
            @Override
            public void onFileChanged(final File mfile, final String name) {
                /**
                 * call the method below any file change
                 * call ur own code here for respond to files changed
                 */
                updateMyWebView(fileToString(mfile));

            }
        }, END_POINT, SOCKET_PORT, HTTP_PORT
                /** The parameters above is that FileWatcher need to know where find ur file */
        );
    }

    private void manageMPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 10101);
        }
    }

    private void inflateViews() {
        contentView = (WebView) findViewById(R.id.content_view);
        setupWebView(contentView);
    }

    private void setupWebView(WebView webView) {
        webView.getSettings().setJavaScriptEnabled(true);
        //displayTextFromUrl("");
    }

    private void displayText(String fileName) {

        currentFile = fileName;

        String url = url(fileName);

        if (TextUtils.isEmpty(url))
            throw new RuntimeException("L'url pour télécharger le fichier n'as pas été renseigner ");

        // contentView.loadUrl(url);

        if (FileUtils.exist(fileName, this)) {
            updateMyWebView(fileToString(FileUtils.getFile(fileName, this)));
            fileIsShowing = true;
            FileWatcher.saveFileFromUrl(url, fileName, this, new FileWatcher.DownloadCallBack() {
                @Override
                public void onSucces(File file, final String name) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateMyWebView(fileToString(FileUtils.getFile(name, MainActivity.this)));
                        }
                    });

                }

                @Override
                public void onFailed(String error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (!fileIsShowing)
                                findViewById(R.id.placeholder).setVisibility(View.VISIBLE);

                        }
                    });

                }
            });
        } else {
            fileIsShowing = false;
            progresDialog.setMessage(getString(R.string.default_progress_msg));
            progresDialog.show();
            FileWatcher.saveFileFromUrl(url, fileName, this, new FileWatcher.DownloadCallBack() {
                @Override
                public void onSucces(File file, final String name) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progresDialog.dismiss();
                            updateMyWebView(fileToString(FileUtils.getFile(name, MainActivity.this)));
                        }
                    });

                }

                @Override
                public void onFailed(String error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!fileIsShowing)
                                findViewById(R.id.placeholder).setVisibility(View.VISIBLE);
                            progresDialog.dismiss();
                            updateMyWebView("");
                            Toast.makeText(MainActivity.this, "Une erreur survenue lors du téléchargement", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

        }
    }


    private void setupUiFrame() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.main_title));
       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/

        progresDialog = new ProgressDialog(this);

    }

    private void updateMyWebView(final String patch) {
        fileIsShowing = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //execute code on main thread
                findViewById(R.id.content_view).setVisibility(View.VISIBLE);
                findViewById(R.id.placeholder).setVisibility(View.GONE);

                contentView.loadData(patch, "text/html; charset=UTF-8;", null);
                //webview.getSettings().setAllowFileAccess(true);
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            EditDialog editDialog = new EditDialog();
            editDialog.show(this, getSupportFragmentManager(), new EditDialog.PassListener() {
                @Override
                public void isCorrect() {
                    startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                }
            });

            //
            return true;
        }

        else if(id == R.id.action_exit){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    private void buildHeader(boolean compact, Bundle savedInstanceState) {
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.xges1)
                .withCompactStyle(compact)
                .withSavedInstance(savedInstanceState)
                .build();


    }

    private void buildDrawer(Bundle savedInstance) {

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_home).withIcon(R.drawable.ic_home_grey_600_24dp).withIdentifier(12),
                        new ExpandableBadgeDrawerItem().withName(R.string.drawer_item_section_header).withIdentifier(18).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.drawer_item_jour).withLevel(2).withIcon(R.drawable.ic_today_black_24dp).withIdentifier(MVM_JOUR),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_hebdo).withLevel(2).withIcon(R.drawable.ic_view_week_black_24dp).withIdentifier(MVM_HEBDO),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_mensuel).withLevel(2).withIcon(R.drawable.ic_view_agenda_black_24dp).withIdentifier(MVM_MONTH)
                        ),
                        new ExpandableBadgeDrawerItem().withName(R.string.drawer_item_section_header1).withIdentifier(18).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.drawer_item_jour).withLevel(2).withIcon(R.drawable.ic_today_black_24dp).withIdentifier(MVMC_JOUR),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_hebdo).withLevel(2).withIcon(R.drawable.ic_view_week_black_24dp).withIdentifier(MVMC_HBDO),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_mensuel).withLevel(2).withIcon(R.drawable.ic_view_agenda_black_24dp).withIdentifier(MVMC_MONTH)
                        ),
                        new ExpandableDrawerItem().withName(R.string.drawer_item_section_header2).withIdentifier(19).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.drawer_item_jour).withLevel(2).withIcon(R.drawable.ic_today_black_24dp).withIdentifier(V_JOUR),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_hebdo).withLevel(2).withIcon(R.drawable.ic_view_week_black_24dp).withIdentifier(V_HEBDO),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_mensuel).withLevel(2).withIcon(R.drawable.ic_view_agenda_black_24dp).withIdentifier(V_MONTH)
                        ),
                        new ExpandableDrawerItem().withName(R.string.drawer_item_section_header4).withIdentifier(19).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.drawer_item_jour).withLevel(2).withIcon(R.drawable.ic_today_black_24dp).withIdentifier(B_JOUR),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_hebdo).withLevel(2).withIcon(R.drawable.ic_view_week_black_24dp).withIdentifier(B_HEBDO),
                                new SecondaryDrawerItem().withName(R.string.drawer_item_mensuel).withLevel(2).withIcon(R.drawable.ic_view_agenda_black_24dp).withIdentifier(B_MONTH)
                        )
                )
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        MainActivity.this.finish();
                        return true;
                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        //check if the drawerItem is set.
                        //there are different reasons for the drawerItem to be null
                        //--> click on the header
                        //--> click on the footer
                        //those items don't contain a drawerItem

                        if (drawerItem != null) {

                            switch ((int) drawerItem.getIdentifier()){
                                case (int)MVM_JOUR:
                                    displayText(_STOCK_DAY);
                                    break;
                                case (int)MVM_HEBDO:
                                    displayText(_STOCK_HEBDO);
                                    break;
                                case (int) MVM_MONTH:
                                    displayText(_STOCK_MONTH);
                                    break;

                                case  (int) MVMC_JOUR:
                                    displayText(_CAISSE_DAY);
                                    break;
                                case  (int) MVMC_HBDO:
                                    displayText(_CAISSE_HEBDO);
                                    break;
                                case (int) MVMC_MONTH :
                                    displayText(_CAISSE_MONTH);
                                    break;

                                case  (int) B_JOUR:
                                    displayText(_BILAN_DAY);
                                    break;
                                case  (int) B_HEBDO:
                                    displayText(_BILAN_HEBDO);
                                    break;
                                case (int) B_MONTH :
                                    displayText(_BILAN_MONTH);
                                    break;

                                case (int) V_JOUR :
                                    displayText(_VENTE_DAY);
                                    break;
                                case  (int) V_HEBDO :
                                    displayText(_VENTE_HEBDO);
                                    break;
                                case (int) V_MONTH:
                                    displayText(_VENTE_MONTH);
                                    break;

                            }
                        }


                        return false;
                    }
                })
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.drawer_item_help).withIdentifier(2013),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_a_propos).withIdentifier(2014)
                )
                .withSavedInstance(savedInstance)
                .build();
    }


}
