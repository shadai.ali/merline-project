package io.craftman.line.business.config;

/**
 * Created by ALI SHADAÏ (Software Craftman) on 10/03/2017.
 */

public class DefaultFiles {

   public static final String _CAISSE_DAY = "caisseday.txt";
   public static final String _CAISSE_HEBDO = "caissehebdo.txt";
   public static final String _CAISSE_MONTH = "caissemonth.txt";

    public static final String _STOCK_DAY = "stockday.txt";
   public static final String _STOCK_HEBDO = "stockhebdo.txt";
   public static final String _STOCK_MONTH = "stockmonth.txt";

   public static final String _VENTE_DAY = "venteday.txt";
   public static final String _VENTE_HEBDO = "ventehebdo.txt";
   public static final String _VENTE_MONTH = "ventemonth.txt";


   public static final String _BILAN_DAY = "venteday.txt";
   public static final String _BILAN_HEBDO = "ventehebdo.txt";
   public static final String _BILAN_MONTH = "ventemonth.txt";

}
